// Find the latest CentOS Minimal ISO
package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "regexp"
    "strings"
)

var url = "https://mirrors.edge.kernel.org/centos/7/isos/x86_64/"

func getISOName() (link string) {
    // Make HTTP request
    response, err := http.Get(url)
    if err != nil {
        log.Fatal(err)
    }
    defer response.Body.Close()

    // Read response data in to memory
    body, err := ioutil.ReadAll(response.Body)
    if err != nil {
        log.Fatal("Error reading HTTP body. ", err)
    }

    // Create a regular expression to find centos minimal version
    re := regexp.MustCompile(`<a href=.*-Minimal.*iso\">`)
    links := re.FindAllString(string(body), -1)
    if links == nil {
        fmt.Println("No matches.")
        return
    } else {
        for _, link := range links {
                // remove href tag and trailing close
                link := strings.Replace(link, "\">", "", -1)[9:]
                return link
        }
   }
   return link
}

func main() {
        fmt.Println(url+getISOName())

}